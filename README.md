# Léeme (¡importante!)

Aquí encontrarás modelado numérico de sistemas físicos complejos. Para una correcta visualización de los archivos .ipynb, deberás, una vez abierto el mismo, hacer click en la opción "Display rendered file".
Puedes encontrar este ícono a la izquierda del botón "Edit" y a la derecha del botón "Display source", todos localizados en el encabezado del archivo.

Para una mejor visualización de los videos presentes, se recomienda abrirlos en una nueva pestaña, haciendo clic derecho y escogiendo el comando mencionado.


Ante cualquier duda, escribir a lcabral.imd@gmail.com